// testStack.c by *****

#include <stdio.h>
#include "stack.h"
#include "testCommon.h"

void testInitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();


int main() {
    testInitStack();
	return 0;
}

